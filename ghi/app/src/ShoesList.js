import './index.css';

// async function deleteShoe(shoe) {
//     const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`
//     const fetchConfig = {
//         method: 'delete',
//     };
//     await fetch(shoeUrl, fetchConfig)


// }

function ShoesList({shoes, getShoes}) {
  const deleteShoe = async (shoe) => {
    const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`
    const fetchConfig = {
      method: 'delete'
    };
  const response = await fetch(shoeUrl, fetchConfig);
  if (response.ok) {
      getShoes();

  }
  }

    console.log(shoes)
    // const hatArray = [...hats]
    // console.log(hatArray)
    return(
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td> {shoe.manufacturer} </td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>
                    <img
                        src={ shoe.picture_url }
                        alt=""
                        width="75px"
                        height="75px"
                    />
                </td>
                <td>{ shoe.bin }</td>
                <td>
                    <button id={ shoe.id } onClick={() => deleteShoe(shoe)}
                        type="button" className="btn btn-danger">
                        delete
                    </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    )
}
export default ShoesList;
