import React, { useEffect, useState } from 'react';

function ShoesForm() {
    const [bins, setBins] = useState([]);


    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }

    }

////// get our list of bins /////////////////

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          console.log(data);
          setBins(data.bins);
        }
      }

      useEffect(() => {
          fetchData();
      }, []);



////////// jsx ///////////////////////

      return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form onSubmit={handleSubmit} id="create-html-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleManufacturerChange} value={manufacturer} placeholder="manufacturer name" required type="text" name = "style_name" id="style_name" className="form-control"/>
                        <label htmlFor="name">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleModelChange} value={model_name} placeholder="model name" required type="text" name = "model_name" id="model_name" className="form-control" />
                        <label htmlFor="model_name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name = "color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrl} value={picture_url} placeholder="Picture Url" required type="text" name = "picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture Url</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleBinChange} required id="bin" name = "bin" className="form-select" value={bin}>
                            <option>Choose a Bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
      )
}
export default ShoesForm;
