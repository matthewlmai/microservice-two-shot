import React, { useEffect, useState } from 'react';

function HatsForm({getHats}) {
    const [locations, setLocations] = useState([]);


    const [styleName, setStyle] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)

            setStyle('');
            setFabric('');
            setColor('');
            setPictureUrl('');
            setLocation('');
            getHats();
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          console.log(data);
          setLocations(data.locations);
        }
      }

      useEffect(() => {
          fetchData();
      }, []);

      return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={handleSubmit} id="create-html-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleStyleNameChange} placeholder="Style name" required type="text" name = "style_name" id="style_name" className="form-control" value={styleName}/>
                        <label htmlFor="name">Style name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name = "fabric" id="fabric" className="form-control" value={fabric}/>
                        <label htmlFor="start_date">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} placeholder="Color" required type="text" name = "color" id="color" className="form-control" value={color}/>
                        <label htmlFor="end_date">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrl} placeholder="Picture Url" required type="text" name = "picture_url" id="picture_url" className="form-control" value={pictureUrl}/>
                        <label htmlFor="max_presentations">Picture Url</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleLocationChange} required id="location" name = "location" className="form-select" value={location}>
                            <option>Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.href}>
                                        {location.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
      )
}
export default HatsForm;
