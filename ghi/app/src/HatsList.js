import './index.css';

// async function deleteHat(hat) {
//     const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`
//     const fetchConfig = {
//         method: 'delete',
//     };
//     await fetch(hatUrl, fetchConfig);
//     // window.location.reload(true)
//     getHats();
// }

function HatsList({hats, getHats}) {
    const deleteHat = async (hat) => {
        const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`
        const fetchConfig = {
            method: 'delete',
        };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        getHats();
    }
    }

    return(
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Hat's Style</th>
            <th>Location</th>
            <th>Picture</th>
            <th>Color</th>
            <th>Fabric</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.style_name }</td>
                <td>{ hat.location }</td>
                <td>
                    <img
                        src={ hat.picture_url }
                        alt=""
                        width="75px"
                        height="75px"
                    />
                </td>
                <td>{ hat.color }</td>
                <td>{ hat.fabric }</td>
                <td>
                    <button id={ hat.id } onClick={() => deleteHat(hat)}
                        type="button" className="btn btn-danger">
                        delete
                    </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    )
}
export default HatsList;
