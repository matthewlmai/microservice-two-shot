import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatsList from './HatsList';
import Nav from './Nav';
import HatsForm from './HatsForm';
import { useEffect, useState } from 'react';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';

function App() {
  const [hats, setHats] = useState([]);
  const [shoes, setShoes] = useState([]);


  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats';

    const hatsResponse = await fetch(url);

    if (hatsResponse.ok) {
      const data = await hatsResponse.json();
      const hats = data.hats
      setHats(hats);
    }
  }


  const getShoes = async () => {
    const shoesUrl = 'http://localhost:8080/api/shoes/';

    const shoeResponse = await fetch(shoesUrl);

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes
      setShoes(shoes)
    }
  }



  useEffect(() => {getHats(); getShoes()}, [setHats, setShoes]);
  console.log(typeof(hats));

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList hats={hats} getHats={getHats} />} />
          <Route path="/hats/new" element={<HatsForm getHats={getHats} />} />
          <Route path="/shoes/" element={<ShoesList getShoes={getShoes} shoes={shoes} />} />
          <Route path="/shoes/new" element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
