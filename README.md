# Wardrobify

Team:

* Anthony Pham - shoes
* Matthew Mai - hats

## Design

## Shoes microservice

Our models consist of our Shoe Model and our BinVO model. The BinVO model is a value object which reflects the Bin model which lies within the Wardrobe API models (which include a Location Model for the Hats microservice and a Bin model for the Shoes microservice). We will continously ping this existing Wardrobe API via our poller to check for new or updated data from external sources. Because we are only interested on a few variables from the external, Wardrobe API Bin model, our BinVO model is created to represent the data in a consisten and predictable way within the Shoes microservice.

## Hats microservice

Within the wardrobe microservice contains the Location models that the Hats microservice wants information from - information that details the locations of hats. When writing code for the models.py inside of the Hats microservice, we included a foreign key that related to the Value Object of Location (LocationVO) which was then important when writing code to build a context map between the Hats microservice and the wardrobe microservice. Building this relationship, it necessitated the poller.py file which allowed us to update/edit the LocationVO as a value object to hold data without changing the state of the Locations in the wardrobe microservices. Through the poller.py, the hats microservice polls (edits/updates/changes LocationVO) data from Locations in the wardrobe microservice every 60 seconds.
