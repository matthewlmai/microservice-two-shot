import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO

def get_bins():
    # we have to hit the correct api url
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    # we set content variable to the python object from the response.content
    # response is a list of all the bins as a python dictionary with the key as "bins" and the value is a list of bins
    content = json.loads(response.content)
    print(content)
    for bin in content["bins"]:
        # for every single bin in the list of bins (since content["bin"]: is the key in response ^^)
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            # we have to set the defaults in the event that there ISN'T a bin existing at that href
            defaults={
                "closet_name": bin["closet_name"],
                "bin_size": bin["bin_size"],
                "bin_number": bin["bin_number"],
            },
        )


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # we just invoke the function now
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
